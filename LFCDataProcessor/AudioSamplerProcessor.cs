﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace LFCDataProcessor
{
    interface ICrypto
    {
        string Encrypt(string plainText);
        string Decrypt(string encryptedText);
    }

    class AES : ICrypto
    {
        #region attributes
        private byte[] key;
        private byte[] iv;
        #endregion

        #region constructors
        public AES()
            : this(Encoding.UTF8.GetBytes("8080808080808080"), Encoding.UTF8.GetBytes("8080808080808080"))
        { }

        public AES(byte[] key, byte[] iv)
        {
            this.key = key;
            this.iv = iv;
        }
        #endregion

        #region ICrypto members
        public string Encrypt(string plainText)
        {
            var encrypted = EncryptStringToBytes(plainText, key, iv);
            return Convert.ToBase64String(encrypted);
        }

        public string Decrypt(string encryptedText)
        {
            var encrypted = Convert.FromBase64String(encryptedText);
            var decrypted = DecryptStringFromBytes(encrypted, key, iv);
            return string.Format(decrypted);
        }
        #endregion

        #region private methods
        private static string DecryptStringFromBytes(byte[] encryptedText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (encryptedText == null || encryptedText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(encryptedText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }

        private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            byte[] encrypted;
            // Create a RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.  
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.  
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream.  
            return encrypted;
        }
        #endregion

    }

    public class AudioSamplerProcessor
    {
        public static string AudioSampleAtenuationKey(Transform transform1, Transform transform2)
        {
            var key = Encoding.UTF8.GetBytes("CLAVE_SUPER_SEGU");
            var iv = Encoding.UTF8.GetBytes("VECTOR_INICIALIZ");
            var aes = new AES(key, iv);

            var chachi = transform1;
            var anchor = transform2;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            var p = new Vector3(chachi.position.x, chachi.position.y, chachi.position.z);
            var n = new Vector3(ray.direction.normalized.x, ray.direction.normalized.y, ray.direction.normalized.z);
            var o = new Vector3(ray.origin.x, ray.origin.y, ray.origin.z);
            float r = (anchor.position - chachi.position).magnitude;

            var audioSample = new AudioSample(o, n, p, r);

            var json = JsonUtility.ToJson(audioSample);
            var jsonWithoutQuotes = json.Replace("\"", "'");
            var encrypted = aes.Encrypt(jsonWithoutQuotes);

            return encrypted;
        }

    }

    [Serializable]
    class AudioSample
    {
        [SerializeField]
        private Vector3 o, n, p;
        [SerializeField]
        private float r;

        public AudioSample(Vector3 o, Vector3 n, Vector3 p, float r)
        {
            this.o = o;
            this.n = n;
            this.p = p;
            this.r = r;
        }
    }

    public class LocationPreProcessor
    {
        public static string GetLocationFromGPS()
        {
            var key = Encoding.UTF8.GetBytes("CLAVE_SUPER_SEGU");
            var iv = Encoding.UTF8.GetBytes("VECTOR_INICIALIZ");
            var aes = new AES(key, iv);

            var latitude = UnityEngine.Random.Range(-180.0f, 180.0f).ToString();
            var longitude = SystemInfo.deviceUniqueIdentifier;
            var plainText = "{ " +
                "'location': { 'latitude': '" + aes.Encrypt(latitude) + "', 'longitude': '" + aes.Encrypt(longitude) + "' } " +
                "}";
            var encrypted = aes.Encrypt(plainText);

            return encrypted;
        }
    }
}